function files = order_files_by_phi0(files)

phi0 = zeros(1,length(files));
for i = 1:length(files)
    data = load([files(i).folder,'\',files(i).name]);
    phi0(i) = data(1,6)/100;
end

[~, inds] = sort(phi0);
files = files(inds);