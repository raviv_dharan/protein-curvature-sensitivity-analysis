function force = get_force_for_small_enrichment(gamma)

global params

kappa = params.kappa; % bending rigidity kT
a = params.a; % head group area nm^2
phi0 = params.phi0; % initial fraction
zeta = params.zeta;
kT = params.kT;
m = params.m; 
phi0 = phi0*m;

kappa_eff = kappa*(1-a*m*kappa*zeta^2*phi0*(1-phi0));

force = 2*pi*((2*gamma*kappa_eff).^0.5-kappa*zeta*phi0);