function F = get_tether_energy_accounting_for_protein_area(args)

R = args(1);
phi = args(2);
global params
kappa = params.kappa; % bending rigidity kT
a = params.a; % head group area nm^2
gamma = params.gamma; % surface tension kT/nm^2
m = params.m; % ratio between protein area and lipid area
phi0 = params.phi0*m; % change from mole fraction to area fraction
zeta = params.zeta; % intrinsic curvature 1/nm

Fe = R*(phi*log(phi)+(1-phi)*log(1-phi)...
    -(phi0*log(phi0)+(1-phi0)*log(1-phi0))...
    -(phi-phi0)*log(phi0/(1-phi0))...
    ); % enthropy contribution
Fb = kappa*a*m*(1/(2*R)-zeta*phi); % bending energy
Ft = R*gamma*a*m; % tension energy;

F = Fe+Fb+Ft;
