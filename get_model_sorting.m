function sorting = get_model_sorting(func_pars, gamma)
global params
kT = params.kT;
get_energy = @get_tether_energy_accounting_for_protein_area;

params.kappa = func_pars(1);
params.zeta = func_pars(2);
initial_guess_R_phi = [20, params.phi0];

gamma = gamma/kT;
sorting = zeros(size(gamma));

for i = 1:length(gamma)
    params.gamma = gamma(i);
    [solution, ~, ~] = fmincon(get_energy,initial_guess_R_phi,[],[],[],[],...
    zeros(size(initial_guess_R_phi)));
    sorting(i) = solution(2)/params.phi0/params.m;
end
1;
