
from lumicks import pylake
from pathlib import Path
import matplotlib.pyplot as plt
import os
from pathlib import Path
import lumicks.pylake as lum
import numpy as np
import cv2
from lumicks.pylake.detail.image import save_tiff
from matplotlib.colors import LinearSegmentedColormap

def get_scan(file):
   #my_file = pylake.File(file)
   scan = file.scans[str(list(file.scans)[0])]
   return scan

file_directory = Path(r'D:\DATA\CD9\20220202\3\New folder\20220202-182003 Scan 15')
file=r'D:\DATA\CD9\20220202\3\New folder\20220202-182003 Scan 15.h5'
pyfile = pylake.File(file)
scan=get_scan(pyfile)

#scan=scan.rgb_image
if len(scan.green_image.shape)==3:
   frame_number=11
   frame=scan.blue_image[frame_number]
   frame2=scan.green_image[frame_number]
   x1, y1, x2, y2 = 37, 25,129, 50
   image_blue=frame[y1:y2,x1:x2]
   image_green=frame2[y1:y2,x1:x2]
   #image_green[image_green==0]=1
   #image_blue_diveded_by_green=image_blue/image_green
   #np.savetxt(scan.name, image_blue_diveded_by_green, delimiter=",")
   np.savetxt(file_directory.name+' frame '+ str(frame_number)+ 'blue.csv', image_blue, delimiter=",")
   np.savetxt(file_directory.name+' frame '+ str(frame_number)+'green.csv', image_green, delimiter=",")

   red_and_green=scan.rgb_image[11,:,:,:]
   x_pixels = red_and_green.shape[1]
   y_pixels = red_and_green.shape[0]
   PIXEL_SIZE = scan.pixelsize_um[0]
   plt.figure()
   plt.imshow(red_and_green / np.max(red_and_green), extent=[0, x_pixels * PIXEL_SIZE, 0, y_pixels * PIXEL_SIZE])
   square_xs_tether = np.array([x1, x2, x2, x1, x1]) * PIXEL_SIZE
   square_ys_tether = np.array(
      [int(scan.lines_per_frame) - y1, int(scan.lines_per_frame) - y1, int(scan.lines_per_frame) - y2,
       int(scan.lines_per_frame) - y2, int(scan.lines_per_frame) - y1]) * PIXEL_SIZE
   plt.plot(square_xs_tether, square_ys_tether)
   plt.show()


else:
   #scan=scan.rgb_image
   x1, y1, x2, y2 = 200, 55, 250, 105
   image_blue = scan.blue_image[y1:y2, x1:x2]
   image_green = scan.green_image[y1:y2, x1:x2]
   red_and_green=scan.rgb_image
   #image_green[image_green==0]=1
   #image_blue_diveded_by_green = image_blue / image_green
   #np.savetxt("20210825-123905 Scan 38_blue_diveded_by_green.csv", image_blue_diveded_by_green, delimiter=",")
   np.savetxt(file_directory.name + 'blue.csv', image_blue, delimiter=",")
   np.savetxt(file_directory.name + 'green.csv', image_green, delimiter=",")
   # image_green[image_green == 0] = 1
   # image_blue_diveded_by_green = image_blue / image_green
   # np.savetxt("20210902-124319 Scan 49_blue_diveded_by_red.csv", image_blue_diveded_by_green, delimiter=",")

   x_pixels = red_and_green.shape[1]
   y_pixels = red_and_green.shape[0]
   PIXEL_SIZE=scan.pixelsize_um[0]
   plt.figure()

   plt.imshow(red_and_green/np.max(red_and_green), extent=[0, x_pixels * PIXEL_SIZE, 0, y_pixels * PIXEL_SIZE])
   square_xs_tether = np.array([x1, x2, x2, x1, x1]) * PIXEL_SIZE
   square_ys_tether = np.array([int(scan.lines_per_frame)-y1, int(scan.lines_per_frame)-y1, int(scan.lines_per_frame)-y2, int(scan.lines_per_frame)-y2,int(scan.lines_per_frame)-y1]) * PIXEL_SIZE
   plt.plot(square_xs_tether,square_ys_tether)
   plt.show()