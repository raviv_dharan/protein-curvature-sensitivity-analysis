function force = get_model_force(func_pars, gamma)
global params
kT = params.kT;
get_energy = @get_tether_energy_accounting_for_protein_area;
params.kappa = func_pars(1);
params.zeta = func_pars(2);
initial_guess_R_phi = [20, params.phi0];

gamma = gamma/kT;
force = zeros(size(gamma));

for i = 1:length(gamma)
    params.gamma = gamma(i);
    [~, objectiveValue, ~] = fmincon(get_energy,initial_guess_R_phi,[],[],[],[],...
    zeros(size(initial_guess_R_phi)));
    force(i) = objectiveValue*2*pi/params.a/params.m*kT*10^-9*10^12;
end
1;