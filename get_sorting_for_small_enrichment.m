function sorting = get_sorting_for_small_enrichment(gamma)

global params

kappa = params.kappa; % bending rigidity kT
a = params.a; % head group area nm^2
phi0 = params.phi0; % initial fraction
zeta = params.zeta;
m = params.m;
phi0 = phi0*m;

kappa_eff =kappa*(1-a*m*kappa*zeta^2*phi0*(1-phi0));

sorting = 1+(1-phi0)*a*m*kappa*zeta*(2*gamma/kappa_eff).^0.5;