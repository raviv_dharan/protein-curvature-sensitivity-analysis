function [] = plot_energy_landscape(get_energy, Rs, phis)
global params
initial_guess_R_phi = [20, params.phi0*params.m];

F = zeros(length(Rs), length(phis));
for i = 1:length(Rs)
    for j = 1:length(phis)
        F(i,j) = get_energy([Rs(i);phis(j)])*2*pi*params.a*1000;
    end
end
[Rs, phis] = meshgrid(Rs, phis);
figure; hold on
surf(Rs, phis, F');
xlabel('R')
ylabel('\phi_t')
zlabel('F')
set(gca, 'Xscale','log')
set(gca, 'Yscale','log')
set(gca, 'Zscale','log')
set(gca, 'ColorScale','log')
a=colorbar;
caxis([10^4,10^7])
ylabel(a,'F (k_BT per \mum)','FontSize',16,'Rotation',270);
a.Label.Position(1) = 4.5;
view(0,90)
set(gca, 'FontSize', 14)
title(['\zeta = ',num2str(params.zeta),'nm^{-1}, \kappa = ', num2str(params.kappa), 'kT, \gamma = ', num2str(params.gamma), 'kT/nm^2'])
[solution,objectiveValue, exitflag] = fmincon(get_energy,initial_guess_R_phi,[],[],[],[],...
        zeros(size(initial_guess_R_phi)));
if objectiveValue > 0
    scatter3(solution(1), solution(2), objectiveValue*2*pi*params.a*1000,'filled','Cdata',[1,0,0])
else
    scatter3(solution(1), solution(2), 0.0001,'filled','Cdata',[1,0,0])
end