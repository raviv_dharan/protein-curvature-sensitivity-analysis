function R = get_tether_radius_for_small_enrichment(gamma)

global params

kappa = params.kappa; % bending rigidity kT
a = params.a; % head group area nm^2
phi0 = params.phi0; % initial fraction
zeta = params.zeta;
m = params.m;
phi0 = phi0*m;

kappa_eff =kappa*(1-a*m*kappa*zeta^2*phi0*(1-phi0));

R = (kappa_eff/2./gamma).^0.5;