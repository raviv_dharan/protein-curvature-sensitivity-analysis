function [] = fitting_for_main_text(protein)

main_folder = 'exp data\';
results_folder = 'results\';
files = dir(fullfile([main_folder,protein], '*.csv'));
info.protein = protein;
info.results_folder = results_folder;
info.num_of_vesicles = length(files);
info.save = true;
if strcmp(protein,'CD9')
    color1 = [0.00,0.45,0.74];
    color2 = [0.00,0.00,1.00];
else
    color1 = [0.93,0.69,0.13];
    color2 = [1.00,0.41,0.16];
end
files = order_files_by_phi0(files);


colors = parula(length(files));
for i = 1:length(files)
    vesicle_num(i) = str2double(files(i).name(1:end-4));
    if vesicle_num(i) == 8
        data = load([files(i).folder,'\',files(i).name]);
        [beta, Sigma] = fit_model_to_exp_data(data);
        gamma = data(:,1);
        force = data(:,2)*10^12;
        force_err = data(:,3)*10^12;
        sorting = data(:,4);
        sorting_err = data(:,5);

        figure(); hold on
        errorbar(sqrt(gamma), force, force_err,'Color',color1, 'LineWidth', 2, 'LineStyle', 'None', 'DisplayName', [protein, ' exp'])
        plot(sqrt(gamma), get_model_force(beta, gamma),'Color',color2, 'LineWidth', 2, 'LineStyle', '--',  'DisplayName', [protein, ' fit']);
        xlabel('$\sqrt{\gamma/(N/m)}$', 'Interpreter', 'latex');
        ylabel('Force (pN)')
        legend('show','Location','northwest', 'EdgeColor','w')
        set(gca, 'FontSize', 14)
        box on
        saveas(gcf,[results_folder, protein,'\',protein,' force main text'])
        saveas(gcf,[results_folder, protein,'\',protein,' force main text.png'])

        figure(); hold on
        errorbar(gamma, sorting, sorting_err,'Color',color1, 'LineWidth', 2, 'LineStyle', 'None', 'DisplayName', [protein, ' exp'])
        plot(gamma, get_model_sorting(beta, gamma),'Color',color2, 'LineWidth', 2, 'LineStyle', '--', 'DisplayName', [protein, ' fit']);
        xlabel('\gamma (N/m)');
        ylabel('Sorting')
        legend('show','Location','northwest', 'EdgeColor','w')
        set(gca, 'FontSize', 14)
        box on
        saveas(gcf,[results_folder, protein,'\',protein,' sorting main text'])
        saveas(gcf,[results_folder, protein,'\',protein,' sorting main text.png'])

        figure(); hold on
        plot(gamma, get_model_radius(beta,gamma),'Color',color2, 'LineWidth', 2, 'LineStyle', '--', 'DisplayName', [protein, ' model results'])
        xlabel('\gamma (N/m)');
        ylabel('R_{tether} (nm)')
        legend('show','Location','northwest', 'EdgeColor','w')
        set(gca, 'FontSize', 14)
        box on
        saveas(gcf,[results_folder, protein,'\',protein,' radius main text'])
        saveas(gcf,[results_folder, protein,'\',protein,' radius main text.png'])

    end
end
