% fit all files to model and save results
%% set up variables
main_folder = 'exp data\';
results_folder = 'results\';
protein = 'CD9';
files = dir(fullfile([main_folder,protein], '*.csv'));
info.protein = protein;
info.results_folder = results_folder;
info.num_of_vesicles = length(files);
info.save = true;

files = order_files_by_phi0(files);

kappa = zeros(size(files));
zeta = zeros(size(files));
kappa_err = zeros(size(files));
zeta_err = zeros(size(files));
phi0 = zeros(size(files));
vesicle_num = zeros(size(files));
figure(100); clf; hold on

radius_x = 35;
sorting_at_radius_x = zeros(size(files));
sorting_error_at_radius_x = zeros(size(files));

colors = parula(length(files));
%% run over files and fit each to model
for i = 1:length(files)
    % get data from file
    data = load([files(i).folder,'\',files(i).name]);
    phi0(i) = data(1,6)/100;
    % fit data to model
    [beta, Sigma] = fit_model_to_exp_data(data);
    
    kappa(i) = beta(1);
    zeta(i) = beta(2);
    kappa_err(i) = Sigma(1);
    zeta_err(i) = Sigma(2);
    vesicle_num(i) = i;
    info.title = files(i).name(1:end-4);
    info.file_num = i;

    plot_fits(data, beta, info);
    
    gamma = data(:,1);
    sorting = data(:,4);
    sorting_err = data(:,5);
    radius = get_model_radius(beta, gamma);
    if radius_x > min(radius) && radius_x < max(radius)
        sorting_at_radius_x(i) = interp1(radius, sorting, radius_x);
        sorting_error_at_radius_x(i) = sqrt(interp1(radius, sorting_err.^2, radius_x));
    end
    figure(100); hold on;
    errorbar(radius,sorting,sorting_err,'Color',colors(i,:),'LineWidth', 2, 'DisplayName',['\phi_0 = ', num2str(phi0(i)*100,'%.3f'),'%'])
end
xlabel('R_{tether} (nm)')
ylabel('Sorting')
figure(100)
set(gca, 'FontSize',14)
box on
legend('show','EdgeColor','w')

figure(200); hold on
errorbar(phi0(sorting_at_radius_x > 0)*100, sorting_at_radius_x(sorting_at_radius_x > 0),...
    sorting_error_at_radius_x(sorting_at_radius_x > 0),'DisplayName', protein, 'LineWidth', 2, 'LineStyle','none')
ylabel('Sorting')
xlabel('\phi_0 (\%)')
legend('show')
set(gca, 'FontSize', 14)
box on

VesicleNum = vesicle_num;
Kappa_in_kT = kappa;
Kappa_err_in_kT = kappa_err;
Zeta_in_1_over_nm = zeta;
Zeta_err_in_1_over_nm = zeta_err;
Phi0 = phi0;
T = table(VesicleNum,Phi0,Kappa_in_kT,Kappa_err_in_kT,Zeta_in_1_over_nm,Zeta_err_in_1_over_nm);

writetable(T,[results_folder,info.protein,'\','results.csv'],'Delimiter',',');

% plot_sorting_vs_phi(files,kappa, zeta, protein, results_folder)
