function [] = plot_fits(data, beta, info)
if strcmp(info.protein, 'TSPAN4')
    fig_begin = 100;
else
    fig_begin = 0;
end
gamma = data(:,1);
force = data(:,2)*10^12;
force_err = data(:,3)*10^12;
sorting = data(:,4);
sorting_err = data(:,5);

figure(fig_begin+(info.file_num)*2-1); clf; hold on
errorbar(sqrt(gamma), force, force_err, 'LineWidth', 2, 'DisplayName', 'data')
plot(sqrt(gamma), get_model_force(beta, gamma), 'LineWidth', 2, 'DisplayName', 'fit');
xlabel('$\sqrt{\gamma (N/m)}$', 'Interpreter', 'latex');
ylabel('Force (pN)')
title(num2str(info.file_num))
legend('show','Location','northwest','EdgeColor', 'w')
set(gca, 'FontSize', 14)
box on
% set(gcf,'Position',[489 343 420 315])
if info.save
    saveas(gcf,[info.results_folder,info.protein,'\',num2str(info.file_num), ' force'])
    saveas(gcf,[info.results_folder,info.protein,'\',num2str(info.file_num), ' force.png'])
end

figure(fig_begin+(info.file_num)*2); clf; hold on
errorbar(gamma, sorting, sorting_err, 'LineWidth', 2, 'DisplayName', 'data')
plot(gamma, get_model_sorting(beta, gamma), 'LineWidth', 2, 'DisplayName', 'fit');
xlabel('$\gamma (N/m)$','Interpreter', 'latex');
ylabel('Sorting')
title(num2str(info.file_num))
legend('show','Location','northwest','EdgeColor', 'w')
set(gca, 'FontSize', 14)
box on
% set(gcf,'Position',[489 343 420 315])

if info.save
    saveas(gcf,[info.results_folder,info.protein,'\',num2str(info.file_num), ' sorting'])
    saveas(gcf,[info.results_folder,info.protein,'\',num2str(info.file_num), ' sorting.png'])

end