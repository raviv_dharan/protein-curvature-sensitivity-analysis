results_folder = 'C:\Users\User\Desktop\Lab\graphs_cuvature_article\model\results';

global  params
params.kappa = 35; %bending rigidity kT
params.a = 0.7; % head group area nm^2
params.m = 13.7;
params.gamma = 0.3; % surface tension kT/nm^2
params.phi0 = 0.0009; % initial mole fraction
params.zeta = 1/10; % intrinsic curvature 1/nm
kT = 4.11*10^-21*10^18; % Kg*nm^2/sec^2
params.kT = kT;

get_energy = @get_tether_energy_accounting_for_protein_area;
initial_guess_R_phi = [20, params.phi0];


Rs = logspace(-1, 3);
phis = logspace(-6,0);
plot_energy_landscape(get_energy, Rs, phis)


gamma = linspace(0.002,0.02,20); % kT/nm^2
phi0 = linspace(0.000005, 0.0005, 4);
R_out = zeros(length(gamma), length(phi0));
phi_out = zeros(length(gamma), length(phi0));

for i = 1:length(gamma)
    params.gamma = gamma(i);
    for j = 1:length(phi0)
        params.phi0 = phi0(j);
        [solution,objectiveValue, exitflag] = fmincon(get_energy,initial_guess_R_phi,[],[],[],[],...
        zeros(size(initial_guess_R_phi)));
        R_out(i,j) = solution(1);
        phi_out(i,j) = solution(2);
        f_out(i,j) = objectiveValue;
    end
end

exp_data = load('test_data_tspn4.csv');
exp_gamma = exp_data(:,1);
exp_force = exp_data(:,2)*10^12;
exp_sorting = exp_data(:,4);



figure(10); clf; hold on; 
for j = 1:length(phi0)
    params.phi0 = phi0(j);
    sc = scatter(gamma*kT, R_out(:,j),'filled', 'DisplayName', ['\phi_0 = ', num2str(phi0(j))]);
    plot(gamma*kT, get_tether_radius_for_small_enrichment(gamma),'Color', sc.CData, 'DisplayName', ['\phi_0 = ', num2str(phi0(j))], 'LineWidth',2)
end
xlabel('\gamma (N/m)');
ylabel('R_{tether} (nm)')
% legend('show','Orientation','horizontal','NumColumns',2)
set(gca, 'FontSize', 14)
box on
saveas(gcf,[results_folder,'\','model radius vs gamma for different phi0'])
saveas(gcf,[results_folder,'\','model radius vs gamma for different phi0.png'])


figure(11); clf; hold on; 
for j = 1:length(phi0)
    params.phi0 = phi0(j);
    sc = scatter(R_out(:,j), phi_out(:,j)/params.phi0/params.m, 'filled', 'DisplayName', ['\phi_0 = ', num2str(phi0(j))]);
    plot(get_tether_radius_for_small_enrichment(gamma), get_sorting_for_small_enrichment(gamma),'Color', sc.CData, 'DisplayName', ['\phi_0 = ', num2str(phi0(j))], 'LineWidth',2)
end
ylabel('Sorting');
xlabel('R_{tether} (nm)')
% legend('show','Orientation','horizontal','NumColumns',2)
set(gca, 'FontSize', 14)
box on
saveas(gcf,[results_folder,'\','model sorting vs radius for different phi0'])
saveas(gcf,[results_folder,'\','model sorting vs radius for different phi0.png'])


figure(20); clf; hold on; 
for j = 1:length(phi0)
    params.phi0 = phi0(j);
    sc = scatter(gamma*kT, phi_out(:,j)/params.phi0/params.m,'filled', 'DisplayName', ['\phi_0 = ', num2str(phi0(j))]);
    plot(gamma*kT, get_sorting_for_small_enrichment(gamma),'Color', sc.CData, 'DisplayName', ['\phi_0 = ', num2str(phi0(j))], 'LineWidth',2)
end
% scatter(exp_gamma, exp_sorting, 'filled', 'DisplayName', 'exp')
xlabel('\gamma (N/m)');
ylabel('Sorting')
% legend('show','Orientation','horizontal','NumColumns',2)
set(gca, 'FontSize', 14)
box on
saveas(gcf,[results_folder,'\','model sorting vs gamma for different phi0'])
saveas(gcf,[results_folder,'\','model sorting vs gamma for different phi0.png'])

figure(30); clf; hold on; 
for j = 1:length(phi0)
    params.phi0 = phi0(j);
    sc = scatter((gamma*kT).^0.5, f_out(:,j)*2*pi/params.a/params.m*kT*10^-9*10^12,... %factor 10^-9 is for conversion from force in Kg*nm/s^2 to N. 10^12 for force in pN
        'filled', 'DisplayName', ['\phi_0 = ', num2str(phi0(j))]); 
    plot((gamma*kT).^0.5, get_force_for_small_enrichment(gamma)*kT*10^-9*10^12,'Color', sc.CData, 'DisplayName', ['\phi_0 = ', num2str(phi0(j))], 'LineWidth',2)
end
% scatter(sqrt(exp_gamma), exp_force, 'filled', 'DisplayName', 'exp')
xlabel('$\sqrt{\gamma (N/m)}$', 'Interpreter', 'latex');
ylabel('Force (pN)')
% legend('show','Orientation','horizontal','NumColumns',2)
set(gca, 'FontSize', 14)
box on
saveas(gcf,[results_folder,'\','model force vs gamma for different phi0'])
saveas(gcf,[results_folder,'\','model force vs gamma for different phi0.png'])




