function [beta, Sigma] = fit_model_to_exp_data(data)
%define model parmameters
global params
kT = 4.11*10^-21*10^18; % Kg*nm^2/sec^2
params.kT = kT;
params.a = 0.7; % head group area nm^2
params.m = 13.7;


gamma = data(:,1);
force = data(:,2)*10^12;
force_err = data(:,3)*10^12;
sorting = data(:,4);
sorting_err = data(:,5);
params.phi0 = data(1,6)/100;

x_cell = {gamma, gamma};
y_cell = {force, sorting};
weights = {1./force_err, 1./sorting_err};
mdl_cell = {@get_model_force, @get_model_sorting};
beta0 = [20, 1/10];
[beta,r,J,Sigma,mse,errorparam,robustw] = nlinmultifit(x_cell, y_cell, mdl_cell, beta0, 'Weights',weights);
1
% params.gamma = gamma(1)/kT;
% Rs = logspace(-1, 3);
% phis = logspace(-9,0);
% plot_energy_landscape(@get_tether_energy_accounting_for_protein_area, Rs, phis)
