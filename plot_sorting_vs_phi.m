function [] = plot_sorting_vs_phi(files,kappa, zeta, protein, results_folder)
global params
radius1 = 35;
radius2 = 50;
sorting_radius1 = zeros(1,length(kappa));
sorting_radius2 = zeros(1,length(kappa));
sorting_err_radius1 = zeros(1,length(kappa));
sorting_err_radius2 = zeros(1,length(kappa));

for i = 1:length(kappa)
    data = load([files(i).folder,'\',files(i).name]);
    gamma = data(:,1);
    sorting = data(:,4);
    sorting_err = data(:,5);
    params.phi0 = data(1,6)/100;
    phi0(i) = params.phi0;
    radius = get_model_radius([kappa(i), zeta(i)], gamma);
    if radius1 >= min(radius) && radius1 <= max(radius)
        sorting_radius1(i) = interp1(radius, sorting, radius1);
        sorting_err_radius1(i) = interp1(radius, sorting_err, radius1);
    else
        sorting_radius1(i) = nan;
        sorting_err_radius1(i) = nan;
    end

    if radius2 >= min(radius) && radius2 <= max(radius)
        sorting_radius2(i) = interp1(radius, sorting, radius2);
        sorting_err_radius2(i) = interp1(radius, sorting_err, radius2);
    else
        sorting_radius2(i) = nan;
        sorting_err_radius2(i) = nan;
    end    
end
figure(22);clf; hold on
errorbar(100*params.m*phi0(~isnan(sorting_radius1)), sorting_radius1(~isnan(sorting_radius1)), sorting_err_radius1(~isnan(sorting_radius1)), 'LineStyle','None', 'LineWidth', 2)
title([protein, ', R_{tether} = ',num2str(radius1),' nm'])
xlabel('\phi_0 (%)')
ylabel('Sorting')
set(gca, 'Fontsize', 14)
box on;
saveas(gcf,[results_folder,protein,'\','sorting vs phi r', num2str(radius1)])
saveas(gcf,[results_folder,protein,'\','sorting vs phi r', num2str(radius1),'.png'])

figure(23);clf; hold on
errorbar(phi0(~isnan(sorting_radius2)), sorting_radius2(~isnan(sorting_radius2)), sorting_err_radius2(~isnan(sorting_radius2)), 'LineStyle','None', 'LineWidth', 2)
title([protein, ', R_{tether} = ',num2str(radius2),' nm'])
xlabel('\phi_0 (%)')
ylabel('Sorting')
set(gca, 'Fontsize', 14)
box on
saveas(gcf,[results_folder,protein,'\','sorting vs phi r', num2str(radius2)])
saveas(gcf,[results_folder,protein,'\','sorting vs phi r', num2str(radius2),'.png'])

1;