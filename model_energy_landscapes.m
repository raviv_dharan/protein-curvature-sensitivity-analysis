results_folder = 'G:\My Drive\Ph.D\projects\enrichment\results\';

global  params
params.a = 0.7; % head group area nm^2
params.m = 13.7;
params.gamma = 0.01; % surface tension kT/nm^2
params.phi0 = 0.00005; % initial mole fraction
kT = 4.11*10^-21*10^18; % Kg*nm^2/sec^2
params.kT = kT;

get_energy = @get_tether_energy_accounting_for_protein_area;
initial_guess_R_phi = [20, params.phi0*params.m];
Rs = logspace(0, 3);
phis = logspace(-6,0);


params.zeta = 1/100;
params.kappa = 10;
plot_energy_landscape(get_energy, Rs, phis)
savefig(gcf, [results_folder,'energy landscape zeta ', num2str(params.zeta),' kappa ',num2str(params.kappa), ' gamma ', num2str(params.gamma),'.fig'])


params.zeta = 1/5;
params.kappa = 10;
plot_energy_landscape(get_energy, Rs, phis)
savefig(gcf, [results_folder,'energy landscape zeta ', num2str(params.zeta),' kappa ',num2str(params.kappa), ' gamma ', num2str(params.gamma),'.fig'])

params.zeta = 1/100;
params.kappa = 50;
plot_energy_landscape(get_energy, Rs, phis)
savefig(gcf, [results_folder,'energy landscape zeta ', num2str(params.zeta),' kappa ',num2str(params.kappa), ' gamma ', num2str(params.gamma),'.fig'])

params.zeta = 1/5;
params.kappa = 50;
plot_energy_landscape(get_energy, Rs, phis)
savefig(gcf, [results_folder,'energy landscape zeta ', num2str(params.zeta),' kappa ',num2str(params.kappa), ' gamma ', num2str(params.gamma),'.fig'])

params.zeta = 1/5;
params.kappa = 50;
params.gamma = 0.03;
plot_energy_landscape(get_energy, Rs, phis)
savefig(gcf, [results_folder,'energy landscape zeta ', num2str(params.zeta),' kappa ',num2str(params.kappa), ' gamma ', num2str(params.gamma),'.fig'])

params.zeta = 1/5;
params.kappa = 50;
params.gamma = 0.05;
plot_energy_landscape(get_energy, Rs, phis)
savefig(gcf, [results_folder,'energy landscape zeta ', num2str(params.zeta),' kappa ',num2str(params.kappa), ' gamma ', num2str(params.gamma),'.fig'])

