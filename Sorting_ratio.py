import csv
from pathlib import Path
from dataclasses import dataclass

import numpy as np
from lumicks import pylake
from matplotlib import pyplot as plt


@dataclass
class ChannelStats:
    photon_sum: float
    density: float
    threshold: int


@dataclass
class ScanStats:
    blue: ChannelStats
    green: ChannelStats


PIXEL_SIZE = 0.1



# this function found the right threshold
def otsus_threshold(matrix):
    bins_num = np.max(matrix) + 1
    hist, bin_edges = np.histogram(matrix, bins=bins_num)
    # Calculate centers of bins
    bin_mids = (bin_edges[:-1] + bin_edges[1:]) / 2
    # Iterate over all thresholds (indices) and get the probabilities w1(t), w2(t)
    weight1 = np.cumsum(hist)
    weight2 = np.cumsum(hist[::-1])[::-1]
    # Get the class means mu0(t)
    mean1 = np.cumsum(hist * bin_mids) / weight1
    # Get the class means mu1(t)
    mean2 = (np.cumsum((hist * bin_mids)[::-1]) / weight2[::-1])[::-1]
    inter_class_variance = weight1[:-1] * weight2[1:] * (mean1[:-1] - mean2[1:]) ** 2
    # Maximize the inter_class_variance function val
    index_of_max_val = np.argmax(inter_class_variance)
    threshold = bin_mids[:-1][index_of_max_val]
    print("Otsu's algorithm implementation thresholding result: ", threshold)

    return threshold


def process_single_channel(channel) -> ChannelStats:
    threshold = otsus_threshold(channel)
    
    channel[channel < threshold] = 0
    area = np.sum(channel > 0)
    photon_sum = np.sum(channel)
    return ChannelStats(photon_sum=photon_sum, density=photon_sum/area if area > 0 else 0, threshold=threshold)



if __name__ == '__main__':
    file_directory = Path(r'\20210513\scans\20210513-182527 Scan 14.h5')
    file_name = r'C:\Users\User\Desktop\Lab\TSPN\data_to_analyze\20210513\scans\20210513-182527 Scan 14.h5'
    file = pylake.File(file_name)

