import matplotlib.pyplot
from lumicks import pylake
from pathlib import Path
import matplotlib.pyplot as plt
import os
from pathlib import Path
import numpy as np
import cv2
from lumicks.pylake.detail.image import save_tiff
from matplotlib.colors import LinearSegmentedColormap
def time_in_sec_from_start(time_in_ns_from_epoch,start_time):
   return 10 ** -9 * (time_in_ns_from_epoch - start_time)

file_directory = Path(r'D:\DATA\CD9\20220202\3\20220202-182003 Marker 9.h5')
file_name = r'D:\DATA\CD9\20220202\3\20220202-182003 Marker 9.h5'
file = pylake.File(file_name)

time= time_in_sec_from_start(file.downsampled_force1x.timestamps, file.downsampled_force1x.timestamps[0])
force=file.downsampled_force1x.data
np.savetxt(file_directory.name+'time.csv', time, delimiter=",")
np.savetxt(file_directory.name+'force.csv', force, delimiter=",")

# plt.plot(time, force)
# for item in file.scans:
#     scan=file.scans[item]
#     scan_start=scan.start
#     scan_stop=scan.stop
#     ind = 0
#     scan_force = np.array([])
#     for i in time:
#         if i > scan_start and i < scan_stop:
#             scan_force=np.append(scan_force,force[ind])
#         ind = ind + 1
#
#     average_force=round(np.average(scan_force),3)
#     plt.plot((scan_start,scan_start),(np.max(file.force1x.data),np.min(file.force1x.data)))
#     plt.plot((scan_stop, scan_stop), (np.max(file.force1x.data), np.min(file.force1x.data)))
#     plt.text(scan_start,np.max(file.force1x.data),item+ str(average_force))
#
# plt.show()